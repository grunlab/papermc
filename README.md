[![pipeline status](https://gitlab.com/grunlab/papermc/badges/main/pipeline.svg)](https://gitlab.com/grunlab/papermc/-/commits/main)

# GrunLab PaperMC

PaperMC non-root container image and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/papermc.md
- https://docs.grunlab.net/apps/papermc.md

Base image: [grunlab/base-image/debian:11][base-image]

Format: docker

Supported architecture(s):
- amd64

[base-image]: <https://gitlab.com/grunlab/base-image>

