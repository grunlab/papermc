#!/bin/bash

JVM_MEMORY=${JVM_MEMORY:-1000M}

# json files
FILES="banned-ips.json banned-players.json ops.json usercache.json whitelist.json"
for FILE in $FILES; do
  if [ ! -f /mnt/papermc-config/$FILE ]; then echo -n '[]' > /mnt/papermc-config/$FILE; fi
  ln -s /mnt/papermc-config/$FILE /var/lib/papermc/$FILE
done

# yml files
FILES="commands.yml bukkit.yml paper.yml permissions.yml spigot.yml"
for FILE in $FILES; do
  if [ ! -f /mnt/papermc-config/$FILE ]; then touch /mnt/papermc-config/$FILE; fi
  ln -s /mnt/papermc-config/$FILE /var/lib/papermc/$FILE
done

# .tweaks
if [ ! -f /mnt/papermc-config/.tweaks ]; then
cat << EOF > /mnt/papermc-config/.tweaks
gc-type=auto
enable-graal=true
EOF
fi
ln -s /mnt/papermc-config/.tweaks /var/lib/papermc/.tweaks

# server.properties
if [ ! -f /mnt/papermc-config/server.properties ]; then touch /mnt/papermc-config/server.properties; fi
ln -s /mnt/papermc-config/server.properties /var/lib/papermc/server.properties

# plugins
if [ ! -d /mnt/papermc-config/plugins ]; then mkdir /mnt/papermc-config/plugins; fi
ln -s /mnt/papermc-config/plugins /var/lib/papermc/plugins

# world
if [ ! -d /mnt/papermc-world/world ]; then mkdir /mnt/papermc-world/world; fi
ln -s /mnt/papermc-world/world /var/lib/papermc/world

# world_nether
if [ ! -d /mnt/papermc-world/world_nether ]; then mkdir /mnt/papermc-world/world_nether; fi
ln -s /mnt/papermc-world/world_nether /var/lib/papermc/world_nether

# world_the_end
if [ ! -d /mnt/papermc-world/world_the_end ]; then mkdir /mnt/papermc-world/world_the_end; fi
ln -s /mnt/papermc-world/world_the_end /var/lib/papermc/world_the_end

# start server
#java -Xmx${JVM_MEMORY} -Xms${JVM_MEMORY} -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:-OmitStackTraceInFastThrow -XX:+AlwaysPreTouch  -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=8 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=true -Daikars.new.flags=true -jar papermc.jar nogui
java -Xmx${JVM_MEMORY} -Xms${JVM_MEMORY} -jar papermc.jar nogui
